#!/usr/bin/python

import sys
import re
class Repeat:
    def __init__(self,name,pre,post,repeat,pat,rep):
        self.name = name
        self.pre = re.compile(pre)
        self.revPre = re.compile(revcomplement(pre))
        self.post = re.compile(post)
        self.revPost = re.compile(revcomplement(post))
        self.repeat = repeat
        self.numbers = []
        pat = pat.replace('.fa','')
        self.pat = pat
        self.minRep = int(rep)
        self.on = 0
        self.off = 0
        self.oddResults = 0
    def onOffRead (self,distance):
        if self.minRep >= int(distance/4.0):
            return False
        repeats = distance/4.0
        if repeats%3 ==0:
            return True
        return False
    def checkRead (self, sequence):
        preRe = self.pre.search(sequence)
        revPreRe = self.revPre.search(sequence)
        if preRe != None:
            postRe = self.post.search(sequence)
            if postRe != None:
                distance = float(postRe.start()) - float(preRe.end())
                if distance%4.0 > 0:
                    self.oddResults += 1
                while distance%4.0 >0:
                    distance = distance -1
                onOff = self.onOffRead(distance)
                if onOff == True:
                    self.on +=1
                else:
                    self.off +=1
                self.numbers.append(int(distance/4.0))
        elif revPreRe != None:
            revPostRe = self.revPost.search(sequence)
            if revPostRe != None:
                distance = float(revPreRe.start()) - float(revPostRe.end())
                if distance%4.0 > 0:
                    self.oddResults += 1
                while distance%4.0 >0:
                    distance = distance -1
                onOff = self.onOffRead(distance)
                if onOff == True:
                    self.on +=1
                else:
                    self.off +=1
                self.numbers.append(int(distance/4.0))
    def getStats (self):
        total = 0
        histogram = {}

        for x in self.numbers:
            if histogram.has_key(x) == False:
                histogram[x] = 0
            histogram[x]+= 1
        ky = histogram.keys()
        ky.sort()
        string = ""
        for x in ky:
            string += "%s=>%s " %(x, histogram[x])
        for t in self.numbers:
            total += float(t)
        try:
            print "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" %(self.pat,self.name,self.repeat,len(self.numbers),max(self.numbers),min(self.numbers),100.0*(float(self.oddResults)/len(self.numbers)),total/len(self.numbers),string,self.on,self.off)
        except:
            print "NA for %s, %s"%(self.pat, self.name)

class Read:
    def __init__(self,header):
        self.name = header.replace('>','')
        self.sequence = ''
    def add (self,seq):
        seq = seq.strip('\n')
        self.sequence += seq

def revcomplement(s):
    bc = {'A':'T', 'C':'G','G':'C','T':'A'}
    l = list(s)
    l = [bc[base] for base in l]
    l.reverse()
    return ''.join(l)
def process(read,repeats):
    for r in repeats:
        r.checkRead(read.sequence)
repeats = []
r = open(sys.argv[2])
for line in r:
    line = line.strip('\n')
    cols = line.split(',')
    sys.argv[3] = sys.argv[3].replace('RL00','')
    sys.argv[3] = sys.argv[3].replace('RL0','')
    sys.argv[3] = sys.argv[3].replace('.sff.fa','')
    if len(cols) >=4:
        repeats.append(Repeat(cols[0],cols[2],cols[3],cols[1],sys.argv[3],cols[4]))
r.close()
fh = open(sys.argv[1])
thisRead = None
print "patient,gene,repeat,counts,max repeats,min repeats,percent odd,Average lenght,histogram,on count, off counts"

for line in fh:
    if line.startswith('>'):
        if thisRead != None:
            process(thisRead,repeats)
        thisRead = Read(line)
    else:
        thisRead.add(line)
for r in repeats:
    r.getStats()
